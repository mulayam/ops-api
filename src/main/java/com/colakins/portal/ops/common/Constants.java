package com.colakins.portal.ops.common;

public class Constants {

    public static final String API_BASE_CONTEXT_PATH = "/portal/ops";

    public static final String API_SIZE_GUARANTEE_CONTEXT_PATH = API_BASE_CONTEXT_PATH + "/guarantee";

    public static final String STATUS_SUCCESS = "SUCCESS";
    public static final String STATUS_FAILURE = "FAILURE";

    public class Header {
        public static final String USER_ID = "UserId";
    }

    public class Error {
        public static final String FIELD_MISSING = "Field {} is required";
        public static final String UNKNOWN_FIELD = "Field {} is should be cm/inch";
    }

    public class Message {
        public static final String SUCCESSFULLY_SAVED_SAMPLE_DATA_MSG = "Sample info saved successfully.";
    }
}
