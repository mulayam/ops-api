package com.colakins.portal.ops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@PropertySources(value = { @PropertySource("classpath:application.properties") })
public class ColakinsPortalOpsApiApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ColakinsPortalOpsApiApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ColakinsPortalOpsApiApplication.class);
	}

}
