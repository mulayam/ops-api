package com.colakins.portal.ops.service.model;

import java.util.Date;

@lombok.Data
public class Node {
	String type;
	Date occurredAt;
	Shop shop;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (shop == null) {
			if (other.shop != null)
				return false;
		} else if (!shop.equals(other.shop))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((shop == null) ? 0 : shop.hashCode());
		return result;
	}

}
