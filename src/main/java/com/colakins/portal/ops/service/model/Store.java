package com.colakins.portal.ops.service.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "stores")

public class Store {
	@Id
	private long id;
	private Integer customerId;
	private Integer apiRetailerId;
	private Integer isValid;
	private Integer canShow;
	private Integer isLead;
	private Integer leadId;
	private Integer isRecord;
	private Integer isNotify;
	private Integer appInstalled;
	private Integer storeStep;
	private String createdAt;
	private String updatedAt;
	private Integer isEnabled;
	private String isUnit;
	private String apiStatus;
	private String apiStoreId;
	private String isSizeChartAvailable;
	private String storeToken;
	private String storeApiKey;
	private String extraParams;
	private String storeType;
	private String storeApiPassword;
	private String isStoreValidationsAvailable;
	private String scrapJobid;
	private String isAppUninstall;
	private String storeName;
	private String storeUrl;
	private String logoUrl;
	private String country;
	private String platform;
	private String style;
	private String primaryColor;
	private String textColor;
	private String fontFamily;
	private String isPhoto;
	private String isConsent;
	private String isLabel;

}
