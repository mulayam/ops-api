package com.colakins.portal.ops.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.persistence.criteria.Predicate;
import javax.persistence.metamodel.MapAttribute;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.colakins.portal.ops.api.model.response.StoreResponse;
import com.colakins.portal.ops.repositories.StoreRepository;
import com.colakins.portal.ops.service.model.Detail;
import com.colakins.portal.ops.service.model.Edge;
import com.colakins.portal.ops.service.model.ShopifyResponse;
import com.colakins.portal.ops.service.model.ShopifyResult;
import com.colakins.portal.ops.service.model.Store;
import com.colakins.portal.ops.service.model.User;

@Service
public class StoreServiceImpl implements StoreService {

	@Autowired
	private StoreRepository storeRepository;

	@Autowired
	private ShopifyStoreExternalCommand shopifyStoreExternalCommand;

	private CompletableFuture<ShopifyResponse> initShopifyGraphApi() {

		return CompletableFuture.supplyAsync(new Supplier<ShopifyResponse>() {
			@Override
			public ShopifyResponse get() {
				return shopifyStoreExternalCommand.callStoreGraphql();
			}
		});

	}

	@Override
	public List<StoreResponse> getFilteredStores(Map<String, Object> map) {
		List<StoreResponse> storeList = null;
		System.out.println("Entered in getFilteredStores() method");
		long startTime = System.currentTimeMillis();
		CompletableFuture<ShopifyResponse> shopifyResponseFuture = null;
		if ("Shopify".equalsIgnoreCase(map.get("platform") + "")) {
			shopifyResponseFuture = initShopifyGraphApi();
		}

		List<Predicate> predicates = new ArrayList<>();
		Specification<Store> specification = (root, query, criteriaBuilder) -> {
			map.forEach((k, v) -> {
				if (v != "" && !(k.equalsIgnoreCase("type") || k.equalsIgnoreCase("occuredDate")
						|| k.equalsIgnoreCase("myshopifyDomain") || k.equalsIgnoreCase("shopId")))
					predicates.add(criteriaBuilder.equal(root.get(k), v));
			});
			return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
		};
		List<Store> list = storeRepository.findAll(specification);

		if ("Shopify".equalsIgnoreCase(map.get("platform") + "")) {
			try {
				ShopifyResponse shopifyResponse = shopifyResponseFuture.get();
				storeList = list.stream().map(store -> {
					StoreResponse response = new StoreResponse();

					// to copy props of source bean to destination bean (of diff type)
					BeanUtils.copyProperties(store, response);
					List<Edge> edges = shopifyResponse.getData().getApp().getEvents().getEdges();
					if (edges != null && edges.size() > 0) {
						Optional<Edge> optional = edges.stream()
								.filter(e -> e.getNode().getShop().getName().equalsIgnoreCase(store.getStoreName()))
								.findFirst();
						if (optional.isPresent()) {
							Edge edge = optional.get();
							Detail details = new Detail();
							details.setType(edge.getNode().getType());
							details.setOccurredAt(edge.getNode().getOccurredAt());
							details.setMyshopifyDomain(edge.getNode().getShop().getMyshopifyDomain());
							details.setShopId(edge.getNode().getShop().getId());
							response.setDetails(details);
						}
					}
					return response;
				}).collect(Collectors.toList());

			} catch (InterruptedException | ExecutionException e) {
				System.err.println(e);
			}
		} else {
			storeList = list.stream().map(store -> {
				StoreResponse response = new StoreResponse();
				BeanUtils.copyProperties(store, response);
				return response;
			}).collect(Collectors.toList());
		}

		// check if expected fitter on details props

		if (map.get("type") != null || map.get("occuredDate") != null || map.get("myshopifyDomain") != null
				|| map.get("shopId") != null) {
			storeList = storeList.stream().filter(t -> {
				String type = (String) map.get("type");
				Date occuredDate = (Date) map.get("occuredDate");
				String domain = (String) map.get("myshopifyDomain");
				String shopId = (String) map.get("shopId");
				Detail d = t.getDetails();
				if (d != null && (type != null && type.equalsIgnoreCase(d.getType()))
						|| (domain != null && domain.equalsIgnoreCase(d.getMyshopifyDomain()))
						|| (shopId != null && shopId.equalsIgnoreCase(d.getShopId()))
						|| (occuredDate != null && occuredDate.compareTo(d.getOccurredAt()) == 0))
					return true;
				else
					return false;
			}).collect(Collectors.toList());
		}
		System.out.println("Total Time:" + (System.currentTimeMillis() - startTime));
		System.out.println("Exited from getFilteredStores() method");

		return storeList;
	}

}
