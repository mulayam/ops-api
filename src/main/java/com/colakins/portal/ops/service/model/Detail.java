package com.colakins.portal.ops.service.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Detail {
	private String type;
	private Date occurredAt;
	private String myshopifyDomain;
	private String shopId;
}
