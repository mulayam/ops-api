package com.colakins.portal.ops.service.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ShopifyResult {
	App app;
}
