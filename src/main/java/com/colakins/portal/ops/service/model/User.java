package com.colakins.portal.ops.service.model;

import lombok.*;

import java.util.List;

@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

	private String keyCloakUserId;

	private String gender, country;

	private List<String> attributes;

	// roles and groups
}
