package com.colakins.portal.ops.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.colakins.portal.ops.service.model.ShopifyResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * This class provides functionalities to call shopify api
 *
 */
@Component
public class ShopifyStoreExternalCommand {

	@Value("${shopify.url}")
	private String url;

	@Value("${shopify.token}")
	private String token;

	@Autowired
	private Environment env;
	@Autowired
	private RestTemplate restTemplate;

	public ShopifyResponse callStoreGraphql() {

		HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.CONTENT_TYPE, "application/graphql");
		headers.set("X-Shopify-Access-Token", token);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		String request = request = "{\n" + "  app(id: \"gid://partners/App/4483741\") {\n" + "    id\n" + "    name\n"
				+ "    events(\n" + "      first: 100\n"
				+ "      types: [RELATIONSHIP_INSTALLED, RELATIONSHIP_UNINSTALLED]\n"
				+ "      occurredAtMin: \"2021-01-01T00:00:00Z\"\n" + "      occurredAtMax: \"2021-04-30T00:00:00Z\"\n"
				+ "    ) {\n" + "      edges {\n" + "        node {\n" + "          type\n" + "          occurredAt\n"
				+ "          shop {\n" + "            id\n" + "            name\n" + "            myshopifyDomain\n"
				+ "          }\n" + "        }\n" + "      }\n" + "    }\n" + "  }\n" + "}\n" + "";

		HttpEntity<String> entity = new HttpEntity<String>(request, headers);

		String data = restTemplate.postForObject(url, entity, String.class);

		ObjectMapper mapper = new ObjectMapper();
		ShopifyResponse root = null;
		try {
			root = mapper.readValue(data, ShopifyResponse.class);
			System.err.println(root);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return root;
	}
}
