package com.colakins.portal.ops.service;

import java.util.List;
import java.util.Map;

import com.colakins.portal.ops.api.model.response.StoreResponse;
import com.colakins.portal.ops.service.model.Store;
import com.colakins.portal.ops.service.model.User;

public interface StoreService {
	public List<StoreResponse> getFilteredStores(Map<String, Object> map);
}
