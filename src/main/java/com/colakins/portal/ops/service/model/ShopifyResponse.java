package com.colakins.portal.ops.service.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ShopifyResponse {
	ShopifyResult data;

}