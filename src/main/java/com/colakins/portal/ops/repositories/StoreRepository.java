package com.colakins.portal.ops.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.colakins.portal.ops.service.model.Store;

import java.util.List;

@Repository
public interface StoreRepository extends JpaRepository<Store, Integer>, JpaSpecificationExecutor<Store> {
	public List<Store> findByPlatform(String platform);
}
