package com.colakins.portal.ops.configuration;

import com.colakins.portal.ops.annotation.WithUser;
import com.colakins.portal.ops.common.Constants;
import com.colakins.portal.ops.service.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

@Slf4j
@Component
public class WithUserHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

    // TODO: 31/05/21 Update this resolver config to get userType, roles and groups (All ops api only accessable to ops team/ admin)

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        // We check if our parameter is exactly what we need:
        return parameter.hasParameterAnnotation(WithUser.class) &&
                parameter.getParameterType().equals(User.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        //take our userId from request header and find the user from keycloak
        String userId = nativeWebRequest.getHeader(Constants.Header.USER_ID);
        /*if (userId != null) {
            log.info("Trying to find the user in keycloak using userId={}", userId);
            // fetch user profile required to allow Api access
            if (user == null) {
                throw new ApiException("Please update user profile.", HttpStatus.BAD_REQUEST, "400");
            }
            return user;
        } else {
            throw new ApiException("Forbidden user .", HttpStatus.FORBIDDEN, "401");
        }*/
        return userId;
    }
}