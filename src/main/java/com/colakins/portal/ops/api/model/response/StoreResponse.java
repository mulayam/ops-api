package com.colakins.portal.ops.api.model.response;

import javax.persistence.Transient;

import com.colakins.portal.ops.service.model.Detail;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StoreResponse {

	private Integer customerId;
	private Integer apiRetailerId;
	private Integer isValid;

	private Integer appInstalled;
	private Integer storeStep;
	private String createdAt;
	private String updatedAt;
	private Integer isEnabled;

	private String apiStatus;
	private String apiStoreId;
	private String isSizeChartAvailable;

	private String storeType;

	private String isAppUninstall;
	private String storeName;
	private String storeUrl;
	private String logoUrl;
	private String country;
	private String platform;

	private Detail details;
}
