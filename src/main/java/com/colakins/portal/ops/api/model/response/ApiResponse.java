package com.colakins.portal.ops.api.model.response;

import java.io.Serializable;

import com.colakins.portal.ops.exception.ApiException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ApiResponse implements Serializable {

	private String status;
	private Object data;
}
