package com.colakins.portal.ops.api;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.colakins.portal.ops.api.model.response.ApiResponse;
import com.colakins.portal.ops.api.model.response.Project;
import com.colakins.portal.ops.api.model.response.StoreResponse;
import com.colakins.portal.ops.common.Constants;
import com.colakins.portal.ops.exception.StoresNotFoundForSpecificPlateformException;
import com.colakins.portal.ops.service.StoreService;
import com.colakins.portal.ops.service.model.Store;
import com.colakins.portal.ops.service.model.User;

import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/ops")
@Log4j2
public class ApplicationApi {

	@Autowired
	private StoreService storeService;

	@Value("${project.name}")
	private String name;

	@Value("${project.version}")
	private String version;

	@GetMapping(path = { "/version" })
	public Project getVersion() {

		log.info("Checking Health point...{}", "portal-ops-api");

		Project project = new Project();
		project.setVersion(version);
		project.setName(name);

		return project;
	}

	/**
	 * Fetch All Stores Data and specific plateform If plateform is not specified in
	 * path then get all stores
	 * 
	 * @return the JSON String
	 */

	@ApiOperation("Return the list of all store and")
	@GetMapping("/guarantee/stores")
	public ApiResponse getFilteredStores(@RequestParam Map<String, Object> map) {
		log.debug("filter params:{}" + map);
		System.err.println(map);
		List<StoreResponse> resultList;
		resultList = storeService.getFilteredStores(map);
		if (resultList != null && resultList.size() == 0) {
			throw new StoresNotFoundForSpecificPlateformException("Stores Not Available for plateform " + map);
		}
		return ApiResponse.builder().data(resultList).status(Constants.STATUS_SUCCESS).build();

	}
}
