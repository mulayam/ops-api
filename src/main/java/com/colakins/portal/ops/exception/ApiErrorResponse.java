package com.colakins.portal.ops.exception;

import org.springframework.http.HttpStatus;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ApiErrorResponse {

	private static final long serialVersionUID = 1L;
	private final String message;
	private final String httpStatus;
	private final String code;

	public ApiErrorResponse(String message, String httpStatus, String code) {
		super();
		this.message = message;
		this.httpStatus = httpStatus;
		this.code = code;
	}

}
