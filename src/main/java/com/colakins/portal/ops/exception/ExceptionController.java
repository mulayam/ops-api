package com.colakins.portal.ops.exception;

import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.colakins.portal.ops.api.model.response.ApiResponse;

import java.util.HashMap;

@ControllerAdvice
@Slf4j
public class ExceptionController extends ResponseEntityExceptionHandler {
	@ExceptionHandler(value = ApiException.class)
	public ResponseEntity<HashMap> exception(ApiException exception) {
		log.error("Got API exception with code={}, message={}", exception.getCode(), exception.getMessage());
		return new ResponseEntity<>(new HashMap<String, String>() {
			{
				put("message", exception.getMessage());
				put("httpStatus", exception.getHttpStatus().getReasonPhrase());
				put("code", exception.getResponseCode());
			}
		}, HttpStatus.OK);
	}

	@ResponseBody
	@ExceptionHandler(StoresNotFoundForSpecificPlateformException.class)
	public ResponseEntity<Object> handlerStoresNotFoundException(StoresNotFoundForSpecificPlateformException ex,
			WebRequest request) {

		return buildResponse(ex, null, HttpStatus.NO_CONTENT, request);

	}

	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		return buildResponse(ex, headers, status, request);
	}

	private ResponseEntity<Object> buildResponse(Exception ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		log.error("Exception occured:", ex);
		ApiErrorResponse exception = new ApiErrorResponse(ex.getMessage(), Integer.toString(status.value()),
				status.name());
		return ResponseEntity.ok(exception);
	}
}