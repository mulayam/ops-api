/**
 * 
 */
package com.colakins.portal.ops.exception;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class ApiException extends Exception {
	private static final long serialVersionUID = 1L;
	private final HttpStatus httpStatus;
	private final String responseCode;
	private final String code;

	public ApiException(String message, HttpStatus httpStatus, String code) {
		super(message);
		this.httpStatus = httpStatus;
		this.responseCode = Integer.toString(httpStatus.value());
		this.code = code;
	}

	public ApiException(String message, HttpStatus httpStatus) {
		super(message);
		this.httpStatus = httpStatus;
		this.responseCode = Integer.toString(httpStatus.value());
		this.code = null;
	}

}
