package com.colakins.portal.ops.exception;

import lombok.Data;

public class StoresNotFoundForSpecificPlateformException extends RuntimeException {
	private static final long serialVersionUID = -4469092157977337709L;

	public StoresNotFoundForSpecificPlateformException(String message) {
		super(message);
	}
}
